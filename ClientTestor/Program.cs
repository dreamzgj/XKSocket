﻿using System;
using System.Collections.Generic;
using System.Text;
using XKSocket;
using MsgIDLib;

namespace ClientTestor
{
    class Program
    {
        static XKClient xct = null;
        static void Main(string[] args)
        {
            Console.WriteLine("请按回车键创建一个网络连接");
            Console.ReadLine();

            xct = new XKClient();
            xct.OnConnected += skt_OnConnected;
            xct.OnDataRecv += skt_OnDataRecv;
            xct.OnDisConnected += skt_OnDisConnected;
            xct.Connect("127.0.0.1", 10101);

            Console.ReadLine();
        }
        static void skt_OnConnected(object sender)
        {
            Console.WriteLine("成功连接至服务端");

            DataPacket dp = new DataPacket((int)MsgID.Login, 0);
            string uName="dreamzgj"+new Random().Next(0,100).ToString();
            dp.DataBodyList.AddRange(BytesParser.GetBytes(uName));
            dp.DataBodyList.AddRange(BytesParser.GetBytes("MyPwd"));
            xct.Send(dp.ToBytesList().ToArray());

        }
        static void skt_OnDataRecv(object sender, byte[] data)
        {
            DataPacket dp = new DataPacket(data);
            switch (dp.MsgID)
            {
                case (int)MsgID.Login:
                    {
                        BytesParser bp = new BytesParser(dp.DataBodyList.ToArray());
                        string RetMsg = bp.GetString();
                        Console.WriteLine("收到服务端返回信息:" + RetMsg);

                        DataPacket dpData = new DataPacket((int)MsgID.SycData, 0);
                        dpData.DataBodyList.AddRange(BytesParser.GetBytes("SycData:11223344556677889900"));
                        xct.Send(dpData.ToBytesList().ToArray());
                    }break;
                case (int)MsgID.SycData:
                    {
                        BytesParser bp = new BytesParser(dp.DataBodyList.ToArray());
                        string RetMsg = bp.GetString();
                        Console.WriteLine("收到服务端返回信息:" + RetMsg);
                    }break;
                default:
                    break;
            }
        }
        static void skt_OnDisConnected(object sender)
        {

        }
    }
}
