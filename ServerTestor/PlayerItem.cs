﻿using System;
using System.Collections.Generic;
using System.Text;
using XKSocket;
using MsgIDLib;

namespace ServerTestor
{
    public class PlayerItem
    {
        public PlayerItem(Connection conn)
        {
            this.Conn = conn;
        }
        Connection Conn;
        public string uName = string.Empty;
        public string uPwd = string.Empty;

        public void Login(string uName, string uPwd)
        {
            this.uName = uName;
            this.uPwd = uPwd;
            Console.WriteLine("用户:" + uName + " 请求登录");

            DataPacket dp=new DataPacket((int)MsgID.Login,0);
            dp.DataBodyList.AddRange(BytesParser.GetBytes("登录成功"));
            PlayerServer.Instance.Skt.Send(this.Conn.ConnectionID, dp.ToBytesList().ToArray());
        }
        public void SycData(string TestData)
        {
            Console.WriteLine("收到用户:" + this.uName + " SycData数据:");
            Console.WriteLine(TestData);

            DataPacket dp = new DataPacket((int)MsgID.SycData, 0);
            dp.DataBodyList.AddRange(BytesParser.GetBytes("服务端已处理了SycData数据"));
            PlayerServer.Instance.Skt.Send(this.Conn.ConnectionID, dp.ToBytesList().ToArray());
        }
        public void ShowDisConnected()
        {
            Console.WriteLine("用户:" + this.uName + " 网络连接已断开");
        }
    }
}
