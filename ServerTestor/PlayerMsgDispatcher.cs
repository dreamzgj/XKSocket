﻿using System;
using System.Collections.Generic;
using System.Text;
using XKSocket;
using MsgIDLib;

namespace ServerTestor
{
    public class PlayerMsgDispatcher
    {
        PlayerMsgDispatcher() { }
        public static PlayerMsgDispatcher Instance
        {
            get { return GetSingleton.instance; }
        }
        class GetSingleton
        {
            static GetSingleton() { }
            internal static readonly PlayerMsgDispatcher instance = new PlayerMsgDispatcher();
        }

        public void MsgDispatch(PlayerItem PlyItem, byte[] bMsgData)
        {
            DataPacket dp = new DataPacket(bMsgData);
            switch (dp.MsgID)
            {
                case (int)MsgID.Login: { this.Login(PlyItem, dp); } break;
                case (int)MsgID.SycData: { this.SycData(PlyItem, dp); } break;
                default:
                    break;
            }
        }
        void Login(PlayerItem PlyItem, DataPacket dp)
        {
            BytesParser bp = new BytesParser(dp.DataBodyList.ToArray());
            string UserName = bp.GetString();
            string UserPwd = bp.GetString();
            PlyItem.Login(UserName, UserPwd);
        }
        void SycData(PlayerItem PlyItem, DataPacket dp)
        {
            BytesParser bp = new BytesParser(dp.DataBodyList.ToArray());
            string TestData = bp.GetString();
            PlyItem.SycData(TestData);
        }
    }
}
