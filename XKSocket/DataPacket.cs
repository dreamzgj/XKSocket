﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XKSocket
{
    public class DataPacket : EventArgs
    {
        public int MsgID;
        public byte Option;
        public List<byte> DataBodyList = new List<byte>();

        public DataPacket(int msgid, byte option)
        {
            this.MsgID = msgid;
            this.Option = option;
        }
        public DataPacket(byte[] bData)
        {
            BytesParser bp = new BytesParser(bData);
            this.MsgID = bp.GetInt();
            this.Option = bp.GetByte();
            this.DataBodyList.AddRange(bp.GetRangeArray());
        }
        public List<byte> ToBytesList()
        {
            List<byte> bytelist = new List<byte>();
            bytelist.AddRange(BitConverter.GetBytes(this.MsgID));
            bytelist.Add(this.Option);
            if (DataBodyList.Count > 0)
            {
                bytelist.AddRange(DataBodyList.ToArray());
            }
            return bytelist;
        }
    }
}
