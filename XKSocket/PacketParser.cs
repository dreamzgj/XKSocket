﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XKSocket
{
    internal class PacketParser
    {
        public PacketParser(){}
        public List<byte> RevBytesList = new List<byte>();

        public List<byte> GetPacketRev(int ConnID,byte[] buff, int len)
        {
            byte[] btmp = new byte[len];
            Array.Copy(buff, btmp, len);

            //如果超大包 则直接丢掉数据
            if (len >= 4)
            {
                BytesParser bp = new BytesParser(btmp);
                int pktLen = bp.GetInt();
                if (pktLen > XKSktConfig.iMaxPktSize && RevBytesList.Count == 0)
                {
                    return null;
                }
            }

            this.RevBytesList.AddRange(btmp);
            return this.GetPacketRev(ConnID);
        }
        public List<byte> GetPacketRev(int ConnID)
        {
            if (RevBytesList.Count >= 4)
            {
                BytesParser bp = new BytesParser(RevBytesList.ToArray());
                //检查是不是内置默认ping包
                if (RevBytesList.Count >= 12)//如果收到的数据已大于12字节,判断收到数据最前和最后12字节是不是ping包
                {
                    //判断最前12字节
                    BytesParser bpFront = new BytesParser(RevBytesList.ToArray());
                    if (bpFront.GetInt() == 12 && bpFront.GetLong() == 0xFAFAFAFA)
                    {
                        RevBytesList.RemoveRange(0, 12); return null;
                    }
                    //判断最后12字节
                    BytesParser bpBack = new BytesParser(bp.GetLastRangeArray(12));
                    if (bpBack.GetInt() == 12 && bpBack.GetLong() == 0xFAFAFAFA)
                    {
                        RevBytesList.RemoveRange(RevBytesList.Count - 12, 12); return null;
                    }
                }

                int pktLen = bp.GetInt();
                if (pktLen <= RevBytesList.ToArray().Length)
                {
                    List<byte> pktList = new List<byte>();
                    pktList.AddRange(RevBytesList.GetRange(0, pktLen));
                    this.RevBytesList.RemoveRange(0, pktLen);
                    return pktList;
                }
            }
            return null;
        }
    }
}
