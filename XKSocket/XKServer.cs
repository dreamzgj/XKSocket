using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace XKSocket
{
    public delegate void SrvSocketEventHandler(object sender, SktEventArgs e);
    public delegate void SrvDataRecvHandler(object sender, SktDataRecvEventArgs e);
    public delegate void SrvErrorEventHandler(object sender, SocketException error);

    /// <summary>
    /// TCP服务器端
    /// </summary>
    public class XKServer
    {
        public event SrvSocketEventHandler OnConnected = null;
        public event SrvSocketEventHandler OnDisConnected = null;
        public event SrvDataRecvHandler OnDataRecv = null;
        public event SrvErrorEventHandler OnError = null;

        private IPAddress _Ipa;
        private int _Port;
        private Socket _ListenerSkt = null;
        private object _LockObj = new object();
        private Timer _tAliveTimer;

        /// <summary>
        /// 监听端口号
        /// </summary>
        public int Port { get { return this._Port; } }
        /// <summary>
        /// 客户端连接列表
        /// </summary>
        public Hashtable htConnections = new Hashtable();

        public XKServer(int port)
        {
            this._Port = port;
        }
        public XKServer(IPAddress ipa, int port)
        {
            this._Ipa = ipa;
            this._Port = port;
        }
        /// <summary>
        /// 启动socket监听服务
        /// </summary>
        public void Start()
        {
            this._ListenerSkt = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            if (this._Ipa != null)
            {
                this._ListenerSkt.Bind(new IPEndPoint(_Ipa, _Port));
            }
            else
            {
                this._ListenerSkt.Bind(new IPEndPoint(IPAddress.Any, _Port));
            }
            this._ListenerSkt.Listen(10000);
            this.BeginAccept();
            this._tAliveTimer = new Timer(AliveTimer, null, 0, XKSktConfig.iAliveTime);
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="ConnectionId">客户端连接ID</param>
        /// <param name="buffer">数据内容</param>
        public void Send(int ConnectionId, byte[] buffer)
        {
            if (htConnections.ContainsKey(ConnectionId))
            {
                Connection connection = (Connection)htConnections[ConnectionId];

                List<byte> ListBytes = new List<byte>();
                ListBytes.AddRange(BitConverter.GetBytes((UInt32)(buffer.Length + 4)));
                ListBytes.AddRange(buffer);
                try
                {
                    if (connection.Socket != null && connection.Socket.Connected)
                    {
                        connection.Socket.BeginSend(ListBytes.ToArray(),
                            0,
                            ListBytes.ToArray().Length,
                            SocketFlags.None,
                            new AsyncCallback(HandleSendData),
                            connection);
                    }
                }
                catch (ObjectDisposedException)
                {
                    NotifyDisConnectedEvent(connection);
                }
                catch (SocketException x)
                {
                    NotifyErrorEvent(connection, x);
                }
            }
        }
        /// <summary>
        /// 断开客户端连接
        /// </summary>
        /// <param name="ConnectionId">客户端连接ID</param>
        public void Disconnect(int ConnectionId)
        {
            if (htConnections.ContainsKey(ConnectionId))
            {
                Connection connection = (Connection)htConnections[ConnectionId];
                connection.Socket.Close();
                NotifyDisConnectedEvent(connection);
            }
        }
        /// <summary>
        /// 停止socket监听服务
        /// </summary>
        public void Shutdown()
        {
            if (_tAliveTimer != null) { _tAliveTimer.Dispose(); }
            if (_ListenerSkt != null)
            {
                _ListenerSkt.Close();
                lock (this._LockObj)
                {
                    Hashtable htTmp = (Hashtable)htConnections.Clone();
                    foreach (DictionaryEntry entry in htTmp)
                    {
                        Connection client = (Connection)entry.Value;
                        client.Socket.Close();
                    }
                    htConnections.Clear();
                }
            }
        }

        private void BeginAccept()
        {
            this._ListenerSkt.BeginAccept(new AsyncCallback(HandleAccepted), null);
        }
        private void AliveTimer(object obj)
        {
            lock (this._LockObj)
            {
                Hashtable htTmp = (Hashtable)htConnections.Clone();
                foreach (DictionaryEntry de in htTmp)
                {
                    Connection conn = de.Value as Connection;
                    if (conn != null)
                    {
                        if (!conn.Socket.Connected)
                        {
                            //连接已经断开了
                            NotifyDisConnectedEvent(conn);
                        }

                        if (!conn.IsAlive)
                        {
                            //连接Is not alive
                            this.Disconnect(conn.ConnectionID);
                        }
                        else
                        {
                            conn.IsAlive = false;
                        }
                    }
                }
            }
        }
        private void HandleAccepted(IAsyncResult parameter)
        {
            Connection connection = null;
            try
            {
                Socket socket = _ListenerSkt.EndAccept(parameter);
                connection = new Connection(socket);
                connection.IsAlive = true;
                do
                {
                    connection.ConnectionID = ConnectionIDPool.PopID();
                } while (htConnections.ContainsKey(connection.ConnectionID));
                htConnections.Add(connection.ConnectionID, connection);

                this.NotifyConnectedEvent(connection);
                BeginRecvData(connection);
                BeginAccept();
            }
            catch (ObjectDisposedException)
            {
                NotifyDisConnectedEvent(connection);
            }
            catch (SocketException x)
            {
                NotifyErrorEvent(connection, x);
            }
        }
        private void HandleSendData(IAsyncResult parameter)
        {
            Connection connection = (Connection)parameter.AsyncState;
            if (connection.Socket.Connected)
            {
                connection.Socket.EndSend(parameter);
            }
        }
        private void BeginRecvData(Connection connection)
        {
            connection.Socket.BeginReceive(connection.Buffer,
                0,
                connection.Buffer.Length,
                SocketFlags.None,
                new AsyncCallback(HandleRecvData),
                connection);
        }
        private void HandleRecvData(IAsyncResult parameter)
        {
            Connection connection = (Connection)parameter.AsyncState;
            try
            {
                int read = connection.Socket.EndReceive(parameter);
                if (read == 0)
                {
                    NotifyDisConnectedEvent(connection);//表示正常的断开
                }
                else
                {
                    connection.IsAlive = true;
                    List<byte> pktList = connection.Pkt.GetPacketRev(connection.ConnectionID, connection.Buffer, read);
                    while (pktList != null)
                    {
                        this.NotifyDataRecvEvent(connection,pktList.GetRange(4, pktList.Count - 4).ToArray());
                        pktList = connection.Pkt.GetPacketRev(connection.ConnectionID);
                    }
                    BeginRecvData(connection);
                }
            }
            catch (ObjectDisposedException)
            {
                NotifyDisConnectedEvent(connection);
            }
            catch (SocketException x)
            {
                if (x.ErrorCode == 10054)
                {
                    NotifyDisConnectedEvent(connection);
                }
                else
                {
                    NotifyErrorEvent(connection, x);
                }
            }
        }
        private void NotifyConnectedEvent(Connection conn)
        {
            if (OnConnected != null){OnConnected(this, new SktEventArgs(conn));}
        }
        private void NotifyDataRecvEvent(Connection conn, byte[] data)
        {
            if (OnDataRecv != null) { OnDataRecv(this, new SktDataRecvEventArgs(conn, data)); }
        }
        private void NotifyDisConnectedEvent(Connection connection)
        {
            lock (this._LockObj)
            {
                if (htConnections.ContainsKey(connection.ConnectionID))
                {
                    htConnections.Remove(connection.ConnectionID);
                    if (OnDisConnected != null)
                    {
                        OnDisConnected(this, new SktEventArgs(connection));
                        BeginAccept();
                    }
                    ConnectionIDPool.PushID(connection.ConnectionID); //调用完disconn在归还ID
                }
            }
        }
        private void NotifyErrorEvent(Connection connection, SocketException error)
        {
            if(OnError!=null && connection!=null)
            {
                OnError(connection.EndPoint, error);
            }
        }
    }
}