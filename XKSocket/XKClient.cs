using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;

namespace XKSocket
{
    public delegate void CtSocketEventHandler(object sender);
    public delegate void CtDataRecvHandler(object sender, byte[] data);
    public delegate void CtErrorEventHandler(object sender, SocketException error);
    /// <summary>
    /// TCP客户端
    /// </summary>
    public class XKClient
    {
        public event CtSocketEventHandler OnConnected = null;
        public event CtSocketEventHandler OnDisConnected = null;
        public event CtDataRecvHandler OnDataRecv = null;
        public event CtErrorEventHandler OnError = null;

        private Socket _Skt = null;
        private byte[] _DataBuffer;
        private PacketParser _PktParser;                 
        private Timer _tPingTimer = null;                //定时发送心跳包
        private DateTime _dtLastSendTime = DateTime.Now; //上一次发送数据的时间

        public XKClient()
        {
            this._DataBuffer = new byte[XKSktConfig.iBufferSize];
            this._PktParser = new PacketParser();
        }
        /// <summary>
        /// 建立socket连接
        /// </summary>
        /// <param name="ip">IP</param>
        /// <param name="port">端口</param>
        public void Connect(string ip, int port)
        {
            this._Skt = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPEndPoint iep = new IPEndPoint(IPAddress.Parse(ip), port);
            this._Skt.BeginConnect(iep, new AsyncCallback(Connected), this._Skt);
        }
        /// <summary>
        /// 发送数据
        /// </summary>
        /// <param name="buffer">数据内容</param>
        public void Send(byte[] buffer)
        {
            List<byte> ListBytes = new List<byte>();
            ListBytes.AddRange(BitConverter.GetBytes((UInt32)(buffer.Length + 4)));
            ListBytes.AddRange(buffer);
            try
            {
                _Skt.BeginSend(ListBytes.ToArray(),
                    0,
                    ListBytes.ToArray().Length,
                    SocketFlags.None,
                    new AsyncCallback(HandleSendData),
                    _Skt);
            }
            catch (ObjectDisposedException)
            {
                NotifyDisConnectedEvent();
            }
            catch (SocketException x)
            {
                NotifyErrorEvent(x);
            }
        }
        /// <summary>
        /// 关闭socket连接
        /// </summary>
        public void Close()
        {
            try
            {
                if (this._tPingTimer != null) { this._tPingTimer.Dispose(); }
                if (_Skt != null)
                {
                    _Skt.Close();
                    NotifyDisConnectedEvent();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("XKSoket" + ex.Message.ToString());
                Console.WriteLine(ex.StackTrace);
            }
        }

        private void Connected(IAsyncResult iar)
        {
            Socket socket = (Socket)iar.AsyncState;
            try
            {
                socket.EndConnect(iar);
            }
            catch (SocketException x)
            {
                this.NotifyErrorEvent(x);
                return;
            }
            if (socket.Connected)
            {
                //触发连接建立事件
                this.NotifyConnectedEvent();
                //开始接收数据
                this.BeginRecvData(socket);
            }
            this._dtLastSendTime = DateTime.Now;
            this._tPingTimer = new System.Threading.Timer(PingAlive, null, 0, XKSktConfig.iAliveTime);
        }
        private void PingAlive(object obj)
        {
            this.Send(BytesParser.GetBytes((long)0xFAFAFAFA));
            this._dtLastSendTime = DateTime.Now;
        }
        private void BeginRecvData(Socket skt)
        {
            skt.BeginReceive(_DataBuffer,
                0,
                XKSktConfig.iBufferSize,
                SocketFlags.None,
                new AsyncCallback(HandleRecvData),
                skt);
        }
        private void HandleRecvData(IAsyncResult parameter)
        {
            try
            {
                Socket remote = (Socket)parameter.AsyncState;
                int read = remote.EndReceive(parameter);
                if (read == 0)
                {
                    NotifyDisConnectedEvent();//表示正常的断开
                }
                else
                {
                    List<byte> pktList = this._PktParser.GetPacketRev(-1, _DataBuffer, read);
                    while (pktList != null)
                    {
                        this.NofifyDataRecvEvent(pktList.GetRange(4, pktList.Count - 4).ToArray());
                        pktList = this._PktParser.GetPacketRev(-1);
                    }
                    this.BeginRecvData(remote);
                }
            }
            catch (ObjectDisposedException)
            {
                NotifyDisConnectedEvent();
            }
            catch (SocketException x)
            {
                if (x.ErrorCode == 10054)
                {
                    NotifyDisConnectedEvent();
                }
                else
                {
                    NotifyErrorEvent(x);
                }
            }
        }
        private void HandleSendData(IAsyncResult parameter)
        {
            Socket socket = (Socket)parameter.AsyncState;
            socket.EndSend(parameter);
            this._dtLastSendTime = DateTime.Now;
        }
        private void NotifyConnectedEvent()
        {
            if (OnConnected != null) { OnConnected(this); }
        }
        private void NofifyDataRecvEvent(byte[] data)
        {
            if (OnDataRecv != null) { OnDataRecv(this, data); }
        }
        private void NotifyDisConnectedEvent()
        {
            if (OnDisConnected != null) { OnDisConnected(this); }
        }
        private void NotifyErrorEvent(SocketException error)
        {
            if (OnError != null) { OnError(this, error); }
        }
    }
}