using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;
namespace XKSocket
{
    public class SktEventArgs
    {
        public Connection Conn { get; set; }
        public SktEventArgs(Connection connection)
        {
            this.Conn = connection;
        }
    }
}
