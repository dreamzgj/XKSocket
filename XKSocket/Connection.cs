using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace XKSocket
{
    public  class Connection
    {
        private  byte[] _Buffer;
        private  Socket _Socket;
        private IPEndPoint _EndPoint;
        private IPEndPoint _LocalPoint;
        private int _RemotePort;
        private int _LocalPort;
        internal PacketParser Pkt;

        public int ConnectionID;  
        public bool IsAlive = true;

        public Connection(Socket socket)
        {
            _Socket = socket;
            _EndPoint = (IPEndPoint)socket.RemoteEndPoint;
            _LocalPoint = (IPEndPoint)socket.LocalEndPoint;
            this._RemotePort = this._EndPoint.Port;
            this._LocalPort = this._LocalPoint.Port;

            _Buffer = new byte[XKSktConfig.iBufferSize];
            Pkt = new PacketParser();
        }
        internal Socket Socket
        {
            get { return _Socket; }
        }
        public IPEndPoint EndPoint
        {
            get { return _EndPoint; }
        }
        public int RemotePort
        {
            get { return this._RemotePort; }
        }
        public IPEndPoint LocalPoint
        {
            get { return _LocalPoint; }
        }
        public int LocalPort
        {
            get { return this._LocalPort; }
        }
        public byte[] Buffer
        {
            get { return _Buffer; }
        }
    }
}
