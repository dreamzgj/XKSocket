﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XKSocket
{
    public class BytesParser
    {
        protected int startIndex = 0;
        protected byte[] data;

        public BytesParser(byte[] data)
        {
            this.data = data;
        }
        public  byte GetByte()
        {
            byte num = data[this.startIndex];
            this.startIndex++;
            return num;
        }
        public  bool GetBool()
        {
            bool b = BitConverter.ToBoolean(data, this.startIndex);
            this.startIndex++;
            return b;
        }
        public  short GetShort()
        {
            short num = BitConverter.ToInt16(new List<byte> { data[this.startIndex], data[this.startIndex + 1] }.ToArray(), 0);
            this.startIndex += 2;
            return num;
        }
        public  ushort GetUShort()
        {
            ushort num = BitConverter.ToUInt16(new List<byte> { data[this.startIndex], data[this.startIndex + 1] }.ToArray(), 0);
            this.startIndex += 2;
            return num;
        }
        public  int GetInt()
        {
            int num = BitConverter.ToInt32(data, this.startIndex);
            this.startIndex += 4;
            return num;
        }
        public  UInt32 GetUInt()
        {
            UInt32 num = BitConverter.ToUInt32(data, this.startIndex);
            this.startIndex += 4;
            return num;
        }
        public  double GetDouble()
        {
            double num = BitConverter.ToDouble(data, this.startIndex);
            this.startIndex += 8;
            return num;
        }
        public  long GetLong()
        {
            long num = BitConverter.ToInt64(data, this.startIndex);
            this.startIndex += 8;
            return num;
        }
        public  byte[] GetRangeArray(int length)
        {
            byte[] destinationArray = new byte[length];
            Array.Copy(data, this.startIndex, destinationArray, 0, length);
            this.startIndex += length;
            return destinationArray;
        }
        /// <summary>
        /// 获取剩下的字节序
        /// </summary>
        public  byte[] GetRangeArray()
        {
            int len=this.data.Length-this.startIndex;
            byte[] DstArray = new byte[len];
            Array.Copy(data, this.startIndex, DstArray, 0,len);
            this.startIndex += len;
            return DstArray;
        }
        /// <summary>
        /// 获取倒数据len个字节数组
        /// 注:此方法不修改内容索引指针
        /// </summary>
        /// <param name="len"></param>
        /// <returns></returns>
        public  byte[] GetLastRangeArray(int len)
        {
            byte[] DstArray = new byte[len];
            if (data.Length >= len)
            {
                Array.Copy(data, data.Length - len, DstArray, 0, len);
            }
            return DstArray;
        }       
        public  string GetString()
        {
            ushort uShort = (ushort)(this.GetUShort());

            string str = Encoding.GetEncoding("gb2312").GetString(data, this.startIndex, uShort);
            this.startIndex += uShort;
            return str;
        }

        public static byte[] GetBytes(byte num)
        {
            return new byte[] { num };
        }
        public static byte[] GetBytes(bool b)
        {
            return BitConverter.GetBytes(b);
        }
        public static byte[] GetBytes(short num)
        {
            return BitConverter.GetBytes(num);
        }
        public static byte[] GetBytes(int num)
        {
            return BitConverter.GetBytes(num);
        }
        public static byte[] GetBytes(long num)
        {
            return BitConverter.GetBytes(num);
        }
        public static byte[] GetBytes(double num)
        {
            return BitConverter.GetBytes(num);
        }
        public static byte[] GetBytes(ushort num)
        {
            return BitConverter.GetBytes(num);
        }
        public static byte[] GetBytes(string str)
        {
            List<byte> list = new List<byte>();
            byte[] bytes = Encoding.GetEncoding("gb2312").GetBytes(str);
            list.AddRange(GetBytes((ushort)bytes.Length));
            list.AddRange(bytes);
            return list.ToArray();
        }
    }
}
